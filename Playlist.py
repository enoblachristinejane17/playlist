#include <iostream>
#include <list>
using namespace std;

class node
 {
public:
		string	Title;
		string	Artist;
		node*	next;
};

class List
 {
    private:
        node* head;
        node* artist;
	
	public:
			List(void)	
			{head = NULL;}
		
			~List(void);
			
			bool isEmpty()
			{ return head == NULL;}
			
	node* add(int index,string Title, string Artist);
			int Find(string a);
			int Delete(int index);
			void DisplayList(void);
			void Edit(int index, string newTitle, string Artist);
			string viewplaylist(int index);
};
int List::Find(string a)
{
	node* CurrNode	=	head;
	node* CurrArtist =	artist;
	int	CurrIndex	=	1;
	while (CurrNode && CurrNode->Title != a)
	{
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrIndex;
	return 0;
}
int List:: Delete(int index)
{
	node* prevNode	=	NULL;
	node* CurrNode	=	head;
	node* prevArtist = NULL;
	node* CurrArtist = artist;
	int CurrIndex	=	1;
	while (CurrIndex != index)
	{
		prevNode	=	CurrNode;
		CurrNode	=	CurrNode->next;
		prevArtist = CurrArtist;
		CurrArtist = CurrArtist->next;
		CurrIndex++;
	}
	if (CurrNode)
	{
		if(prevNode && prevArtist)
		{
			prevNode->next	=	CurrNode->next;
			prevArtist->next = CurrArtist->next;
			delete CurrNode;
		}
		else
		{
			head	=	CurrNode->next;
			artist = CurrArtist->next;
			delete	CurrNode;
			delete	CurrArtist;
		}
		return	CurrIndex;
	}
	return 0;
}
string List::viewplaylist(int index)
{
	node* CurrNode	=	head;
	int	CurrIndex	=	1;
	while (CurrIndex != index)
	{
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	if (CurrNode) return CurrNode->Title;

}

void List::DisplayList()
{
	int number = 0;
	node* CurrNode	=	head;
	node* CurrArtist = 	artist;
	while (CurrNode != NULL &&CurrArtist != NULL)
	{
		cout<<number+1<<CurrNode->Title<<" ";
		cout<<" "<<"\tby "<<" "<<CurrArtist->Artist<<endl;;
		CurrArtist = CurrArtist->next;
		CurrNode	=	CurrNode->next;
		number++;
	}
}  

node* List::add(int index,string Title, string Artist)
{
	if (index < 0) return NULL;
	int CurrIndex = 1;
	node* CurrNode =head;
	node* CurrArtist = artist;
	while (CurrNode && index > CurrIndex)
	{
		CurrNode	=	CurrNode->next;
		CurrIndex++;
	}
	while (CurrArtist && index > CurrIndex)
	{
		CurrArtist	=	CurrArtist->next;
		CurrIndex++;
	}
	node* newNode =		new		node;
	newNode->Title = 	Title;
	node* newArtist = new node;
	newArtist->Artist = Artist;
	if (index==0)
	{
			newNode->next	=	head;
			head 			=	newNode;
			newArtist->next = artist;
			artist = newArtist;
	}
	else
	{
		newNode->next	=	CurrNode->next;
		CurrNode->next	=	newNode;
		newArtist->next = CurrArtist->next;
		CurrArtist->next = newArtist;
	}
	return newNode;
}


List::~List(void)
{
	node* CurrNode	=	head,	*nextNode	=	NULL;
	while (CurrNode != NULL)
	{
		nextNode	=	CurrNode->next;
		delete	CurrNode;
		CurrNode	=	nextNode;
	}
}

List playlist;



int main()

{
	node* CurrNode;
	string Title,Artist,editName,searchName;
	
	int choice,del,edit,play,playlistSize;
	
    playlist.add(0,"Endlessly","The Cab");
	playlist.add(1,"Ocean","Lady Antebellum");
	
	cout<<"1. Add Music"<<endl;
	cout<<"2. Delete Music"<<endl;
	cout<<"3. Play Music"<<endl;
	cout<<"4.View Playlist "<<endl;
	
	cin>>choice;
	switch(choice)
	{
       	case 1:
			cout<<"Enter Title of the song ";
			cin.ignore();
			getline(cin,Title);
			cout<<"Enter Artist > ";
			getline(cin,Artist);
			playlist.add(0,Title,Artist);
			cout<<"Current Playlist : "<<endl;
			playlist.DisplayList();
		break;
		
		
		case 2:
			playlist.DisplayList();
			cout<<"Delete Music ";
			cin>>del;
			playlist.Delete(del);
			playlist.DisplayList();
		break;
	
		
	
		case 3:
			playlist.DisplayList();
		    cout<<"Choose Music"<<endl;
			cin>>play;
			cout<<"Previous : "<<playlist.viewplaylist(play-1)<<endl;
			cout<<"Now Playing : "<<playlist.viewplaylist(play)<<endl;
			cout<<"Next : "<<playlist.viewplaylist(play+1)<<endl;	
	    break;
		
		
		case 4:
			cout<<"Playlist: "<<endl;
			playlist.DisplayList();
		break;
		default:
			cout<<"Doesn't exist";
		break;
		
	}
}